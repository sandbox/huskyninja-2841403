Path to Body Class v0.1.0

Requirements:
- Drulal 7.x
- Pathauto

This module takes the Pathauto path of the given node, converts it into a string (replacing any slashes with dashes), and then inserts this as a class into the body element of the node.

This is useful when using CSS wildcards for styling sections of your site, but don't want to install and play with the Contex module.

Just install this module like any other, and then start the styling.

This module was created by Huskyninja for SageAge. @Copyleft - All wrongs reserved. 

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.